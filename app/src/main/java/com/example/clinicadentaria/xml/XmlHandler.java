package com.example.clinicadentaria.xml;
import com.example.clinicadentaria.dto.*;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import java.io.StringWriter;

public class XmlHandler {

    public static ClinicaDTO deSerializeXML2ClinicaDTO(String xmlData) {
        ClinicaDTO data = null;
        if(xmlData != null){
            Serializer serializer = new Persister();
            try {
                data = serializer.read(ClinicaDTO.class, xmlData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }


    public static String serializeErroDTO2XML(ErroDTO erroDTO) {
        StringWriter writer = new StringWriter();
        if (erroDTO != null) {
            Serializer serializer = new Persister();
            try {
                serializer.write(erroDTO, writer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return writer.toString();
    }
    public static ErroDTO deSerializeXML2ErroDTO(String xmlData) {
        ErroDTO data = null;
        if(xmlData != null){
            Serializer serializer = new Persister();
            try {
                data = serializer.read(ErroDTO.class, xmlData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public static ListaClinicaParcialDTO deSerializeXML2ListaClinicaParcialDTO(String xmlData) {
        ListaClinicaParcialDTO data = null;
        if(xmlData != null){
            Serializer serializer = new Persister();
            try {
                data = serializer.read(ListaClinicaParcialDTO.class, xmlData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public static String serializeClinicaDTO2XML(ClinicaDTO clinicaDTO) {
        StringWriter writer = new StringWriter();
        if (clinicaDTO != null) {
            Serializer serializer = new Persister();
            try {
                serializer.write(clinicaDTO, writer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return writer.toString();
    }

    public static ListaDentistaParcialDTO deSerializeXML2ListaDentistaParcialDTO(String xmlData) {
        ListaDentistaParcialDTO data = null;
        if(xmlData != null){
            Serializer serializer = new Persister();
            try {
                data = serializer.read(ListaDentistaParcialDTO.class, xmlData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public static DentistaDTO deSerializeXML2DentistaDTO(String xmlData) {
        DentistaDTO data = null;
        if(xmlData != null){
            Serializer serializer = new Persister();
            try {
                data = serializer.read(DentistaDTO.class, xmlData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public static String serializeDentistaDTO2XML(DentistaDTO dentistaDTO) {
        StringWriter writer = new StringWriter();
        if (dentistaDTO != null) {
            Serializer serializer = new Persister();
            try {
                serializer.write(dentistaDTO, writer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return writer.toString();
    }

}
