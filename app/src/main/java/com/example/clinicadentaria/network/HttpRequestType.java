package com.example.clinicadentaria.network;

public enum HttpRequestType {
    GET,
    POST,
    PUT,
    DELETE
}
