package com.example.clinicadentaria.dto;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"nome", "cartaoCidadao", "dataNascimento", "identificacaoDentista"})
@Root(name = "cliente")
public class ClienteDTO extends PessoaDTO {

    @Element(name = "identificacaoDentista")
    private long identificacaoDentista;

    public ClienteDTO(String nome, long cartaoCidadao, DataDTO dataNascimento, long identificacaoDentista){
        super(nome, cartaoCidadao, dataNascimento);
        this.identificacaoDentista = identificacaoDentista;
    }

    public ClienteDTO(){
    }

    public long getIdentificacaoDentista() {
        return identificacaoDentista;
    }

    public void setIdentificacaoDentista(long identificacaoDentista) {
        this.identificacaoDentista = identificacaoDentista;
    }
}
