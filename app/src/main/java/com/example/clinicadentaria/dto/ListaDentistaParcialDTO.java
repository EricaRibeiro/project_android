package com.example.clinicadentaria.dto;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import java.util.ArrayList;

@Root(name = "dentistas")
public class ListaDentistaParcialDTO {

    @ElementList(inline = true, required=false)

    private ArrayList<DentistaDTO> dentistas;

    public ListaDentistaParcialDTO() {
        this.dentistas = new ArrayList<>();
    }

    public ArrayList<DentistaDTO> getDentistas() {
        return dentistas;
    }
    public void setDentistas(ArrayList<DentistaDTO> dentistas) {
        this.dentistas = dentistas;
    }
}
