package com.example.clinicadentaria.dto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"tipoServico", "preco", "data", "hora", "identificacaoCliente", "codigoConsulta"})
@Root(name = "tipoConsulta")
public class ServicoDTO {

    @Element(name = "tipoServico")
    private String tipoServico;
    @Element(name = "preco")
    private double preco;
    @Element(name = "data")
    private DataDTO data;
    @Element(name = "hora")
    private HoraDTO hora;
    @Element(name = "identificacaoCliente")
    private long identificacaoCLiente;
    @Element(name = "codigoConsulta")
    private int codigoConsulta;

    public ServicoDTO(){

    }

    public ServicoDTO(String tipoServico, double preco, DataDTO data, HoraDTO hora, long identificacaoCLiente, int codigoConsulta){
        this.tipoServico = tipoServico;
        this.preco = preco;
        this.data = data;
        this.hora = hora;
        this.identificacaoCLiente = identificacaoCLiente;
        this.codigoConsulta = codigoConsulta;
    }

    public String getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(String tipoServico) {
        this.tipoServico = tipoServico;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public HoraDTO getHora() {
        return hora;
    }

    public void setHora(HoraDTO hora) {
        this.hora = hora;
    }

    public long getIdentificacaoCLiente() {
        return identificacaoCLiente;
    }

    public void setIdentificacaoCLiente(long identificacaoCLiente) {
        this.identificacaoCLiente = identificacaoCLiente;
    }

    public int getCodigoConsulta() {
        return codigoConsulta;
    }

    public void setCodigoConsulta(int codigoConsulta) {
        this.codigoConsulta = codigoConsulta;
    }
}
