package com.example.clinicadentaria.dto;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"identificacaoCliente", "avaliacao"})
@Root(name = "avaliacaoClinica")
public class AvaliacaoDTO {

    @Element(name = "identificacaoCliente")
    private long identificacaoCliente;
    @Element(name = "avaliacao")
    private int avaliacao;

    public AvaliacaoDTO(){

    }

    public AvaliacaoDTO(int avaliacao, long identificacaoCliente){
        this.avaliacao = avaliacao;
        this.identificacaoCliente = identificacaoCliente;
    }

    public long getIdentificacaoCliente() {
        return identificacaoCliente;
    }

    public void setIdentificacaoCliente(long identificacaoCliente) {
        this.identificacaoCliente = identificacaoCliente;
    }

    public int getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(int avaliacao) {
        this.avaliacao = avaliacao;
    }
}
