package com.example.clinicadentaria.dto;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import java.util.ArrayList;

@Root(name = "consultas")
public class ListaConsultaParcialDTO {

    @ElementList(inline = true, required=false)

    private ArrayList<ServicoDTO> servicos;

    public ListaConsultaParcialDTO() {
        this.servicos = new ArrayList<>();
    }

    public ArrayList<ServicoDTO> getServicos() {
        return servicos;
    }
    public void setServicos(ArrayList<ServicoDTO> servicos) {
        this.servicos = servicos;
    }
}
