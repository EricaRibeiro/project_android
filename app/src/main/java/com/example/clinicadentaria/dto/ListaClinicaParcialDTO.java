package com.example.clinicadentaria.dto;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

@Root(name = "clinicas")
public class ListaClinicaParcialDTO {

    @ElementList(inline = true, required=false)

    private ArrayList<ClinicaDTO> clinicas;

    public ListaClinicaParcialDTO() {
        this.clinicas = new ArrayList<>();
    }

    public ArrayList<ClinicaDTO> getClinicas() {
        return clinicas;
    }
    public void setClinicas(ArrayList<ClinicaDTO> clinicas) {
        this.clinicas = clinicas;
    }
}
