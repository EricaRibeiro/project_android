package com.example.clinicadentaria.dto;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"nome", "cartaoCidadao", "data", "numeroCedula"})
@Root(name = "dentista")
public class DentistaDTO extends PessoaDTO {
    @Root(name = "numeroCedula")
    private long numeroCedula;

    public DentistaDTO(String nome, long cartaoCidadao, DataDTO dataNascimento, long numeroCedula){
        super(nome, cartaoCidadao, dataNascimento);
        this.numeroCedula = numeroCedula;
    }

    public DentistaDTO(){
    }

    public long getNumeroCedula() {
        return numeroCedula;
    }

    public void setNumeroCedula(long numeroCedula) {
        this.numeroCedula = numeroCedula;
    }
}
