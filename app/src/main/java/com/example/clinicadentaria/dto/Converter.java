package com.example.clinicadentaria.dto;
import com.example.clinicadentaria.model.*;
import com.example.clinicadentaria.model.partial.ClinicaParcial;
import com.example.clinicadentaria.model.partial.DentistaParcial;
import com.example.clinicadentaria.model.partial.ListaClinicaParcial;
import com.example.clinicadentaria.model.partial.ListaDentistaParcial;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    public static Data dataDTO2Data(DataDTO data){
        return new Data(data.getDia(), data.getMes(), data.getAno());
    }

    public static DataDTO data2DataDTO(Data data){
        return new DataDTO(data.getDia(), data.getMes(), data.getAno());
    }

    public static Hora horaDTO2Hora(HoraDTO hora){
        return new Hora(hora.getHora(), hora.getMinutos());
    }

    public static HoraDTO hora2HoraDTO(Hora hora){
        return new HoraDTO(hora.getHora(), hora.getMinutos());
    }

    public static ListaClinicaParcial listaClinicaParcialDTO2ListaClinicaParcial(ListaClinicaParcialDTO listaClinicaParcialDTO) throws NullPointerException {
        ArrayList<ClinicaParcial> clinicaParcial = new ArrayList<ClinicaParcial>();
        ArrayList<ClinicaDTO> clinicasDTO = listaClinicaParcialDTO.getClinicas();
        if(clinicasDTO != null) {
            for (ClinicaDTO clinicaDTO : clinicasDTO) {
                try {
                    ClinicaParcial clinicaP = clinicaParcialDTO2ClinicaParcial(clinicaDTO);
                    clinicaParcial.add(clinicaP);
                } catch (NullPointerException e) {
                    //does nothing. Actually, nothing is added to arraylist
                }
            }
        }
        ListaClinicaParcial listaClinicaParcial = new ListaClinicaParcial(clinicaParcial);
        return listaClinicaParcial;
    }

    public static ClinicaParcial clinicaParcialDTO2ClinicaParcial(ClinicaDTO clinicaDTO) throws NullPointerException {
        ClinicaParcial clinicaParcial = null;
        clinicaParcial = new ClinicaParcial(clinicaDTO.getNome(), clinicaDTO.getNif());
        return clinicaParcial;
    }

    public static ClinicaDTO clinica2ClinicaDTO(Clinica clinica) throws NullPointerException {
        ClinicaDTO clinicaDTO = new ClinicaDTO();
        clinicaDTO.setNif(clinica.getNif());
        clinicaDTO.setNome(clinica.getNome());
        DataDTO dataDTO = data2DataDTO(clinica.getDataConstituicao());
        clinicaDTO.setDataConstituicao(dataDTO);
        return clinicaDTO;
    }

    public static Clinica clinicaDTO2Clinica(ClinicaDTO clinicaDTO) throws NullPointerException {
        Clinica clinica = null;
        Data data = dataDTO2Data(clinicaDTO.getDataConstituicao());
        clinica = new Clinica(clinicaDTO.getNome(), data, clinicaDTO.getNif());
        return clinica;
    }

    public static DentistaParcial dentistaParcialDTO2DentistaParcial(DentistaDTO dentistaParcialDTO) throws NullPointerException {
        DentistaParcial dentistaParcial = new DentistaParcial();
        dentistaParcial.setNome(dentistaParcialDTO.getNome());
        dentistaParcial.setCc(dentistaParcialDTO.getCartaoCidadao());
        dentistaParcial.setCedula(dentistaParcialDTO.getNumeroCedula());

        return dentistaParcial;
    }

    public static ListaDentistaParcial listaDentistaParcialDTO2ListaDentistaParcial(ListaDentistaParcialDTO listaDentistaParcialDTO) throws NullPointerException {
        ArrayList<DentistaParcial> dentistasParciais = new ArrayList<>();
        ArrayList<DentistaDTO> dentistas = listaDentistaParcialDTO.getDentistas();
        if(dentistas != null) {
            for (DentistaDTO dentistaDTO : dentistas) {
                try {
                    DentistaParcial dentistaParcial = dentistaParcialDTO2DentistaParcial(dentistaDTO);
                    dentistasParciais.add(dentistaParcial);
                } catch (NullPointerException e) {
                    //does nothing. Actually, nothing is added to arraylist
                }
            }
        }
        ListaDentistaParcial listaDentistaParcial = new ListaDentistaParcial();
        listaDentistaParcial.setDentistasParciais(dentistasParciais);
        return listaDentistaParcial;
    }

    public static Dentista dentistaDTO2Dentista(DentistaDTO dentistaDTO) throws NullPointerException {
        Dentista dentista = null;
        Data data = dataDTO2Data(dentistaDTO.getDataNascimento());
        dentista = new Dentista(dentistaDTO.getNome(), dentistaDTO.getCartaoCidadao(), data, dentistaDTO.getNumeroCedula());
        return dentista;
    }

    public static DentistaDTO dentista2DentistaDTO(Dentista dentista) throws NullPointerException {
        DentistaDTO dentistaDTO = new DentistaDTO();
        dentistaDTO.setNumeroCedula(dentista.getNumeroCedula());
        dentistaDTO.setNome(dentista.getNome());
        DataDTO dataDTO = data2DataDTO(dentista.getDataNascimento());
        dentistaDTO.setDataNascimento(dataDTO);
        dentistaDTO.setCartaoCidadao(dentista.getCartaoCidadao());
        return dentistaDTO;
    }
}
