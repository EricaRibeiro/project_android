package com.example.clinicadentaria.model.partial;

public class DentistaParcial {

    private long cc;
    private long cedula;
    private String nome;

    public DentistaParcial(String nome, long cc, long cedula){
        this.cc = cc;
        this.cedula = cedula;
        this.nome = nome;
    }

    public DentistaParcial(){}

    public long getCc() {
        return cc;
    }

    public void setCc(long cc) {
        this.cc = cc;
    }

    public long getCedula() {
        return cedula;
    }

    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
