package com.example.clinicadentaria.model.partial;

import java.util.ArrayList;

public class ListaClinicaParcial {
    private ArrayList<ClinicaParcial> clinicaParciais;

    public ArrayList<ClinicaParcial> getClinicaParciais() {
        return clinicaParciais;
    }

    public void setClinicaParciais(ArrayList<ClinicaParcial> pessoaPartials) {
        this.clinicaParciais = pessoaPartials;
    }

    public ListaClinicaParcial(ArrayList<ClinicaParcial> pessoaPartials) {
        this.clinicaParciais = pessoaPartials;
    }
}
