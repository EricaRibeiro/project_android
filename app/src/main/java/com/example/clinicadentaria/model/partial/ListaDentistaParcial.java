package com.example.clinicadentaria.model.partial;

import java.util.ArrayList;

public class ListaDentistaParcial {
    private ArrayList<DentistaParcial> dentistasParciais;

    public ArrayList<DentistaParcial> getDentistasParciais() {
        return dentistasParciais;
    }

    public void setDentistasParciais(ArrayList<DentistaParcial> pessoaPartials) {
        this.dentistasParciais = pessoaPartials;
    }

    public ListaDentistaParcial(){}

    public ListaDentistaParcial(ArrayList<DentistaParcial> pessoaPartials) {
        this.dentistasParciais = pessoaPartials;
    }
}
