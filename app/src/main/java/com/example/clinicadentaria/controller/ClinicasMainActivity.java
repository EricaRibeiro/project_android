package com.example.clinicadentaria.controller;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.adapter.ListViewAdapterClinica;
import com.example.clinicadentaria.dto.Converter;
import com.example.clinicadentaria.dto.ErroDTO;
import com.example.clinicadentaria.dto.ListaClinicaParcialDTO;
import com.example.clinicadentaria.helper.Response;
import com.example.clinicadentaria.helper.Utils;
import com.example.clinicadentaria.model.partial.ClinicaParcial;
import com.example.clinicadentaria.model.partial.ListaClinicaParcial;
import com.example.clinicadentaria.network.*;
import com.example.clinicadentaria.xml.XmlHandler;
import java.util.ArrayList;


public class ClinicasMainActivity extends AppCompatActivity {

    ListView lv;
    ProgressBar pb;
    ArrayList<ClinicaParcial> clinicasParciais;
    ListViewAdapterClinica adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinicas_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        pb = findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.INVISIBLE);

        lv = findViewById(R.id.listView);
        clinicasParciais = new ArrayList<>();
        adapter = new ListViewAdapterClinica(this, R.layout.listview_clinicas_item, clinicasParciais);
        lv.setAdapter(adapter);
        registerForContextMenu(lv);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(ClinicasMainActivity.this,"Click: details",Toast.LENGTH_SHORT).show();
                ClinicaParcial clinicaParcial = (ClinicaParcial) adapter.getItem(i);
                Intent intent = new Intent(ClinicasMainActivity.this, ClinicaActivity.class);
                intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_DETAILS);
                intent.putExtra(Utils.ID, clinicaParcial.getNif());
                startActivity(intent);
            }
        });
        getClinicasFromWS();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.add:
                Toast.makeText(ClinicasMainActivity.this,"Adicionar",Toast.LENGTH_SHORT).show();
                intent = new Intent(ClinicasMainActivity.this, ClinicaActivity.class);
                intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_ADDING);
                startActivityForResult(intent, Utils.REQUEST_CODE_ADD_ACTIVITY);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int pos = info.position;
        Intent intent;
        ClinicaParcial clinicaParcial = (ClinicaParcial) adapter.getItem(pos);
        if (clinicaParcial != null) {
            switch (item.getItemId()) {
                case R.id.delete:
                    Toast.makeText(ClinicasMainActivity.this,"Apagar",Toast.LENGTH_SHORT).show();
                    intent = new Intent(ClinicasMainActivity.this, ClinicaActivity.class);
                    intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_DELETING);
                    intent.putExtra(Utils.ID, clinicaParcial.getNif());
                    startActivityForResult(intent, Utils.REQUEST_CODE_DELETE_ACTIVITY);
                    return true;
                case R.id.edit:
                    Toast.makeText(ClinicasMainActivity.this,"Editar",Toast.LENGTH_SHORT).show();
                    intent = new Intent(ClinicasMainActivity.this, ClinicaActivity.class);
                    intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_EDITING);
                    intent.putExtra(Utils.ID, clinicaParcial.getNif());
                    startActivityForResult(intent, Utils.REQUEST_CODE_EDIT_ACTIVITY);
                    return true;
                default:
                    break;
            }
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Utils.REQUEST_CODE_ADD_ACTIVITY:
            case Utils.REQUEST_CODE_DELETE_ACTIVITY:
            case Utils.REQUEST_CODE_EDIT_ACTIVITY:
                if (resultCode == Activity.RESULT_OK) {
                    getClinicasFromWS();
                }
                break;
            default:
        }
    }

    private void getClinicasFromWS() {
        AsyncTask<String, Void, Response> task = new AsyncTask<String, Void, Response>() {
            @Override
            protected void onPreExecute() {
                pb.setVisibility(ProgressBar.VISIBLE);
            }
            @Override
            protected Response doInBackground(String... params) {
                Response response = null;
                HttpRequest httpRequest = new HttpRequest(HttpRequestType.GET,params[0],"");
                HttpResponse httpResponse = HttpConnection.makeRequest(httpRequest);
                switch (httpResponse.getStatus()){
                    case  HttpStatusCode.OK:
                        ListaClinicaParcialDTO listaClinicaParcialDTO = XmlHandler.deSerializeXML2ListaClinicaParcialDTO(httpResponse.getBody());
                        ListaClinicaParcial clinicas = Converter.listaClinicaParcialDTO2ListaClinicaParcial(listaClinicaParcialDTO);
                        response = new Response(HttpStatusCode.OK, clinicas);
                        break;
                    case  HttpStatusCode.Conflict:
                        ErroDTO erroDTO = XmlHandler.deSerializeXML2ErroDTO(httpResponse.getBody());
                        response = new Response(HttpStatusCode.Conflict, erroDTO.getMensagemErro());
                        break;
                }
                return response;
            }
            @Override
            protected void onPostExecute(Response result) {
                super.onPostExecute(result);
                clinicasParciais.clear();
                if (result != null) {
                    Object object = result.getBody();
                    switch (result.getStatus()){
                        case  HttpStatusCode.OK:
                            if(object instanceof ListaClinicaParcial) {
                                clinicasParciais.addAll(((ListaClinicaParcial) object).getClinicaParciais());
                                adapter.notifyDataSetChanged();
                            }
                            break;
                        case  HttpStatusCode.Conflict:
                            if(object instanceof String) {
                                String message = (String) object;
                                Toast.makeText(ClinicasMainActivity.this,message,Toast.LENGTH_LONG).show();
                            }
                            break;
                        default:
                            Toast.makeText(ClinicasMainActivity.this,Utils.UNKNOWN_ACTION,Toast.LENGTH_LONG).show();
                            break;
                    }
                }
                pb.setVisibility(ProgressBar.GONE);
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinicas");
    }
}
