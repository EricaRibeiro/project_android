package com.example.clinicadentaria.controller;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.dto.ClinicaDTO;
import com.example.clinicadentaria.dto.Converter;
import com.example.clinicadentaria.dto.DentistaDTO;
import com.example.clinicadentaria.dto.ErroDTO;
import com.example.clinicadentaria.helper.Response;
import com.example.clinicadentaria.helper.Utils;
import com.example.clinicadentaria.model.Clinica;
import com.example.clinicadentaria.model.Data;
import com.example.clinicadentaria.model.Dentista;
import com.example.clinicadentaria.network.*;
import com.example.clinicadentaria.xml.XmlHandler;

import java.util.Calendar;

public class DentistaActivity extends AppCompatActivity {

    int mode;
    long id;
    Dentista dentista;

    EditText etCC, etNome, etCedula;
    DatePicker dp;
    Button btOp, btCancel;
    ProgressBar pb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dentistas);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        dentista = null;
        pb = findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.INVISIBLE);
        etCC = findViewById(R.id.etCC);
        etNome = findViewById(R.id.etNome);
        etCedula = findViewById(R.id.etCedula);

        dp = findViewById(R.id.dp);

        btOp = findViewById(R.id.btOp);
        btOp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long cc, cedula;
                String nome;
                Data data = null;
                String exceptionMessage = "";
                boolean exception = false;
                switch(mode){
                    case Utils.ACTIVITY_MODE_ADDING:
                        try {
                            cc = Long.parseLong(etCC.getText().toString());
                            nome = etNome.getText().toString();
                            data = new Data(dp.getDayOfMonth(), dp.getMonth(), dp.getYear());
                            cedula = Long.parseLong(etCedula.getText().toString());
                            Dentista a = new Dentista(nome, cedula, data, cc);
                            postDentista2WS(a);
                        }catch (Exception e){
                            exceptionMessage = e.getMessage();
                            exception = true;
                        }
                        break;
                    case Utils.ACTIVITY_MODE_DELETING:
                        deleteDentista2WS();
                        break;
                    case Utils.ACTIVITY_MODE_EDITING:
                        try {
                            cc = Long.parseLong(etCC.getText().toString());
                            nome = etNome.getText().toString();
                            data = new Data(dp.getDayOfMonth(),dp.getMonth(),dp.getYear());
                            cedula = Long.parseLong(etCedula.getText().toString());
                            Dentista e = new Dentista(nome, cedula, data, cc);
                            putDentista2WS(e);
                        }catch (Exception e){
                            exceptionMessage = e.getMessage();
                            exception = true;
                        }
                        break;
                    default:
                        Toast.makeText(DentistaActivity.this,Utils.UNKNOWN_MODE,Toast.LENGTH_SHORT).show();
                }
                if(exception == false) {
                    Intent intent = new Intent();
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }else{
                    Toast.makeText(DentistaActivity.this,exceptionMessage,Toast.LENGTH_SHORT).show();
                }

            }
        });
        btCancel = (Button)findViewById(R.id.btCancel);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                setResult(Activity.RESULT_CANCELED, intent);
                finish();
            }
        });

        Intent intent = getIntent();
        mode  = intent.getIntExtra("MODE", Utils.ACTIVITY_MODE_NOTHING);
        configureUI();

        if(mode == Utils.ACTIVITY_MODE_DELETING || mode == Utils.ACTIVITY_MODE_DETAILS || mode == Utils.ACTIVITY_MODE_EDITING) {
            id = intent.getLongExtra("ID", Utils.ID_DEFAULT_VALUE);
            getDentistaFromWS();
        }



    }


    private void configureUI(){
        switch(mode){
            case Utils.ACTIVITY_MODE_ADDING:
                etCC.setEnabled(true);;
                etNome.setEnabled(true);
                dp.setEnabled(true);
                btOp.setText("Inserir");
                break;
            case Utils.ACTIVITY_MODE_DELETING:
                etCC.setEnabled(false);;
                etNome.setEnabled(false);
                dp.setEnabled(false);
                btOp.setText("Eliminar");
                break;
            case Utils.ACTIVITY_MODE_EDITING:
                etCC.setEnabled(true);;
                etNome.setEnabled(true);
                dp.setEnabled(true);
                btOp.setText("Alterar");
                break;
            case Utils.ACTIVITY_MODE_DETAILS:
                etCC.setEnabled(false);
                etNome.setEnabled(false);
                dp.setEnabled(false);
                btOp.setVisibility(View.GONE);
                btCancel.setVisibility(View.GONE);
                break;
            default:
                Toast.makeText(DentistaActivity.this,"Mode: desconhecido",Toast.LENGTH_SHORT).show();
        }
    }
    private void setDataUI(){

        if(mode == Utils.ACTIVITY_MODE_DELETING || mode == Utils.ACTIVITY_MODE_DETAILS || mode == Utils.ACTIVITY_MODE_EDITING){
            if(dentista != null){
                etCC.setText(dentista.getCartaoCidadao()+"");
                etNome.setText(dentista.getNome());
                Data data = dentista.getDataNascimento();
                etCedula.setText(dentista.getNumeroCedula()+"");
                dp.updateDate(data.getAno(), data.getMes(), data.getDia());
            }
            else{
                Toast.makeText(DentistaActivity.this,Utils.OPERATION_NO_DATA,Toast.LENGTH_SHORT).show();
                etCC.setText("");
                etNome.setText("");
                etCedula.setText("");
                int year = Calendar.getInstance().get(Calendar.YEAR);
                int month = Calendar.getInstance().get(Calendar.MONTH);
                int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                dp.updateDate(year, month, day);
            }
        }

    }

    private void getDentistaFromWS() {
        AsyncTask<String, Void, Response> task = new AsyncTask<String, Void, Response>() {
            @Override
            protected void onPreExecute() {
                pb.setVisibility(ProgressBar.VISIBLE);
            }
            @Override
            protected Response doInBackground(String... params) {
                Response response = null;
                HttpRequest httpRequest = new HttpRequest(HttpRequestType.GET,params[0],"");
                HttpResponse httpResponse = HttpConnection.makeRequest(httpRequest);
                switch (httpResponse.getStatus()){
                    case  HttpStatusCode.OK:
                        DentistaDTO dentistaDTO = XmlHandler.deSerializeXML2DentistaDTO(httpResponse.getBody());
                        Dentista dentista = Converter.dentistaDTO2Dentista(dentistaDTO);
                        response = new Response(HttpStatusCode.OK, dentista);
                        break;
                    case  HttpStatusCode.Conflict:
                        ErroDTO erroDTO = XmlHandler.deSerializeXML2ErroDTO(httpResponse.getBody());
                        response = new Response(HttpStatusCode.Conflict, erroDTO.getMensagemErro());
                        break;
                }
                return response;
            }
            @Override
            protected void onPostExecute(Response result) {
                super.onPostExecute(result);
                if (result != null) {
                    Object object = result.getBody();
                    switch (result.getStatus()){
                        case  HttpStatusCode.OK:
                            if(object instanceof Clinica) {
                                dentista = (Dentista) object;
                                setDataUI();
                            }
                            break;
                        case  HttpStatusCode.Conflict:
                            if(object instanceof String) {
                                String message = (String) object;
                                Toast.makeText(DentistaActivity.this,message,Toast.LENGTH_LONG).show();
                            }
                            break;
                        default:
                            Toast.makeText(DentistaActivity.this,Utils.UNKNOWN_ACTION,Toast.LENGTH_LONG).show();
                            break;
                    }
                }
                pb.setVisibility(ProgressBar.GONE);
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/getDentista/"+id);
    }
    private void postDentista2WS(Dentista dentista){
        DentistaDTO dentistaDTO = Converter.dentista2DentistaDTO(dentista);
        final String body =  XmlHandler.serializeDentistaDTO2XML(dentistaDTO);
        AsyncTask<String, Void, Response> task = new AsyncTask<String, Void, Response>() {
            @Override
            protected void onPreExecute() {
                pb.setVisibility(ProgressBar.VISIBLE);
            }
            @Override
            protected Response doInBackground(String... params) {
                Response response = null;
                HttpRequest httpRequest = new HttpRequest(HttpRequestType.POST,params[0],body);
                HttpResponse httpResponse = HttpConnection.makeRequest(httpRequest);
                switch (httpResponse.getStatus()){
                    case  HttpStatusCode.Created:
                        response = new Response(HttpStatusCode.Created, null);
                        break;
                    case  HttpStatusCode.Conflict:
                        ErroDTO erroDTO = XmlHandler.deSerializeXML2ErroDTO(httpResponse.getBody());
                        response = new Response(HttpStatusCode.Conflict, erroDTO.getMensagemErro());
                        break;
                }
                return response;
            }
            @Override
            protected void onPostExecute(Response result) {
                super.onPostExecute(result);
                if (result != null) {
                    Object object = result.getBody();
                    switch (result.getStatus()){
                        case  HttpStatusCode.Created:
                            Toast.makeText(DentistaActivity.this,Utils.OPERATION_ADD_SUCESSS,Toast.LENGTH_LONG).show();
                            break;
                        case  HttpStatusCode.Conflict:
                            if(object instanceof String) {
                                String message = (String) object;
                                Toast.makeText(DentistaActivity.this,message,Toast.LENGTH_LONG).show();
                            }
                        default:
                            Toast.makeText(DentistaActivity.this,Utils.UNKNOWN_ACTION,Toast.LENGTH_LONG).show();
                            break;
                    }
                }
                pb.setVisibility(ProgressBar.GONE);
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "clinica/"+R.id.etNifDentista+"/dentista");
    }
    private void putDentista2WS(Dentista dentista) {
        DentistaDTO dentistaDTO = Converter.dentista2DentistaDTO(dentista);
        final String body =  XmlHandler.serializeDentistaDTO2XML(dentistaDTO);
        AsyncTask<String, Void, Response> task = new AsyncTask<String, Void, Response>() {
            @Override
            protected void onPreExecute() {
                pb.setVisibility(ProgressBar.VISIBLE);
            }
            @Override
            protected Response doInBackground(String... params) {
                Response response = null;
                HttpRequest httpRequest = new HttpRequest(HttpRequestType.PUT,params[0],body);
                HttpResponse httpResponse = HttpConnection.makeRequest(httpRequest);
                switch (httpResponse.getStatus()){
                    case  HttpStatusCode.OK:
                        response = new Response(HttpStatusCode.OK, null);
                        break;
                    case  HttpStatusCode.Conflict:
                        ErroDTO erroDTO = XmlHandler.deSerializeXML2ErroDTO(httpResponse.getBody());
                        response = new Response(HttpStatusCode.Conflict, erroDTO.getMensagemErro());
                        break;
                }
                return response;
            }
            @Override
            protected void onPostExecute(Response result) {
                super.onPostExecute(result);
                if (result != null) {
                    Object object = result.getBody();
                    switch (result.getStatus()){
                        case  HttpStatusCode.OK:
                            Toast.makeText(DentistaActivity.this,Utils.OPERATION_UPDATE_SUCESSS,Toast.LENGTH_LONG).show();
                            break;
                        case  HttpStatusCode.Conflict:
                            if(object instanceof String) {
                                String message = (String) object;
                                Toast.makeText(DentistaActivity.this,message,Toast.LENGTH_LONG).show();
                            }
                        default:
                            Toast.makeText(DentistaActivity.this,Utils.UNKNOWN_ACTION,Toast.LENGTH_LONG).show();
                            break;
                    }
                }
                pb.setVisibility(ProgressBar.GONE);
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "dentista/"+R.id.etNifDentista+"/dentista/"+id);
    }
    private void deleteDentista2WS() {

        AsyncTask<String, Void, Response> task = new AsyncTask<String, Void, Response>() {
            @Override
            protected void onPreExecute() {
                pb.setVisibility(ProgressBar.VISIBLE);
            }
            @Override
            protected Response doInBackground(String... params) {
                Response response = null;
                HttpRequest httpRequest = new HttpRequest(HttpRequestType.DELETE,params[0],"");
                HttpResponse httpResponse = HttpConnection.makeRequest(httpRequest);
                switch (httpResponse.getStatus()){
                    case  HttpStatusCode.OK:
                        response = new Response(HttpStatusCode.OK, null);
                        break;
                    case  HttpStatusCode.Conflict:
                        ErroDTO erroDTO = XmlHandler.deSerializeXML2ErroDTO(httpResponse.getBody());
                        response = new Response(HttpStatusCode.Conflict, erroDTO.getMensagemErro());
                        break;
                }
                return response;
            }
            @Override
            protected void onPostExecute(Response result) {
                super.onPostExecute(result);
                if (result != null) {
                    Object object = result.getBody();
                    switch (result.getStatus()){
                        case  HttpStatusCode.OK:
                            Toast.makeText(DentistaActivity.this,Utils.OPERATION_DELETE_SUCESSS,Toast.LENGTH_LONG).show();
                            break;
                        case  HttpStatusCode.Conflict:
                            if(object instanceof String) {
                                String message = (String) object;
                                Toast.makeText(DentistaActivity.this,message,Toast.LENGTH_LONG).show();
                            }
                        default:
                            Toast.makeText(DentistaActivity.this,Utils.UNKNOWN_ACTION,Toast.LENGTH_LONG).show();
                            break;
                    }
                }
                pb.setVisibility(ProgressBar.GONE);
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica/"+R.id.etNifDentista+"pessoa/"+id);
    }
}
