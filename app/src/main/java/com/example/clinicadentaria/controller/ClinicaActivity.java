package com.example.clinicadentaria.controller;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.dto.ClinicaDTO;
import com.example.clinicadentaria.dto.Converter;
import com.example.clinicadentaria.dto.ErroDTO;
import com.example.clinicadentaria.helper.Response;
import com.example.clinicadentaria.helper.Utils;
import com.example.clinicadentaria.model.Clinica;
import com.example.clinicadentaria.model.Data;
import com.example.clinicadentaria.network.*;
import com.example.clinicadentaria.xml.XmlHandler;

import java.util.Calendar;

public class ClinicaActivity extends AppCompatActivity {

    int mode;
    long id;
    Clinica clinica;

    EditText etNif, etNome;
    DatePicker dp;
    Button btOp, btCancel;
    ProgressBar pb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinicas);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        clinica = null;
        pb = findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.INVISIBLE);
        etNif = findViewById(R.id.etNif);
        etNome = findViewById(R.id.etNome);

        dp = findViewById(R.id.dp);

        btOp = findViewById(R.id.btOp);
        btOp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long nif;
                String nome;
                Data data = null;
                String exceptionMessage = "";
                boolean exception = false;
                switch(mode){
                    case Utils.ACTIVITY_MODE_ADDING:
                        try {
                            nif = Long.parseLong(etNif.getText().toString());
                            nome = etNome.getText().toString();
                            data = new Data(dp.getDayOfMonth(), dp.getMonth(), dp.getYear());
                            Clinica a = new Clinica(nome, data, nif);
                            postClinica2WS(a);
                        }catch (Exception e){
                            exceptionMessage = e.getMessage();
                            exception = true;
                        }
                        break;
                    case Utils.ACTIVITY_MODE_DELETING:
                        deleteClinica2WS();
                        break;
                    case Utils.ACTIVITY_MODE_EDITING:
                        try {
                            nif = Long.parseLong(etNif.getText().toString());
                            nome = etNome.getText().toString();
                            data = new Data(dp.getDayOfMonth(),dp.getMonth(),dp.getYear());
                            Clinica e = new Clinica(nome, data, nif);
                            putClinica2WS(e);
                        }catch (Exception e){
                            exceptionMessage = e.getMessage();
                            exception = true;
                        }
                        break;
                    default:
                        Toast.makeText(ClinicaActivity.this,Utils.UNKNOWN_MODE,Toast.LENGTH_SHORT).show();
                }
                if(exception == false) {
                    Intent intent = new Intent();
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }else{
                    Toast.makeText(ClinicaActivity.this,exceptionMessage,Toast.LENGTH_SHORT).show();
                }

            }
        });
        btCancel = (Button)findViewById(R.id.btCancel);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                setResult(Activity.RESULT_CANCELED, intent);
                finish();
            }
        });

        Intent intent = getIntent();
        mode  = intent.getIntExtra("MODE", Utils.ACTIVITY_MODE_NOTHING);
        configureUI();

        if(mode == Utils.ACTIVITY_MODE_DELETING || mode == Utils.ACTIVITY_MODE_DETAILS || mode == Utils.ACTIVITY_MODE_EDITING) {
            id = intent.getLongExtra("ID", Utils.ID_DEFAULT_VALUE);
            getClinicaFromWS();
        }



    }


    private void configureUI(){
        switch(mode){
            case Utils.ACTIVITY_MODE_ADDING:
                etNif .setEnabled(true);;
                etNome.setEnabled(true);
                dp.setEnabled(true);
                btOp.setText("Inserir");
                break;
            case Utils.ACTIVITY_MODE_DELETING:
                etNif .setEnabled(false);;
                etNome.setEnabled(false);
                dp.setEnabled(false);
                btOp.setText("Eliminar");
                break;
            case Utils.ACTIVITY_MODE_EDITING:
                etNif .setEnabled(true);;
                etNome.setEnabled(true);
                dp.setEnabled(true);
                btOp.setText("Alterar");
                break;
            case Utils.ACTIVITY_MODE_DETAILS:
                etNif .setEnabled(false);
                etNome.setEnabled(false);
                dp.setEnabled(false);
                btOp.setVisibility(View.GONE);
                btCancel.setVisibility(View.GONE);
                break;
            default:
                Toast.makeText(ClinicaActivity.this,"Mode: desconhecido",Toast.LENGTH_SHORT).show();
        }
    }
    private void setDataUI(){

        if(mode == Utils.ACTIVITY_MODE_DELETING || mode == Utils.ACTIVITY_MODE_DETAILS || mode == Utils.ACTIVITY_MODE_EDITING){
            if(clinica != null){
                etNif.setText(clinica.getNif()+"");
                etNome.setText(clinica.getNome());
                Data data = clinica.getDataConstituicao();
                dp.updateDate(data.getAno(), data.getMes(), data.getDia());
            }
            else{
                Toast.makeText(ClinicaActivity.this,Utils.OPERATION_NO_DATA,Toast.LENGTH_SHORT).show();
                etNif.setText("");
                etNome.setText("");
                int year = Calendar.getInstance().get(Calendar.YEAR);
                int month = Calendar.getInstance().get(Calendar.MONTH);
                int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                dp.updateDate(year, month, day);
            }
        }

    }

    private void getClinicaFromWS() {
        AsyncTask<String, Void, Response> task = new AsyncTask<String, Void, Response>() {
            @Override
            protected void onPreExecute() {
                pb.setVisibility(ProgressBar.VISIBLE);
            }
            @Override
            protected Response doInBackground(String... params) {
                Response response = null;
                HttpRequest httpRequest = new HttpRequest(HttpRequestType.GET,params[0],"");
                HttpResponse httpResponse = HttpConnection.makeRequest(httpRequest);
                switch (httpResponse.getStatus()){
                    case  HttpStatusCode.OK:
                        ClinicaDTO clinicaDTO = XmlHandler.deSerializeXML2ClinicaDTO(httpResponse.getBody());
                        Clinica clinica = Converter.clinicaDTO2Clinica(clinicaDTO);
                        response = new Response(HttpStatusCode.OK, clinica);
                        break;
                    case  HttpStatusCode.Conflict:
                        ErroDTO erroDTO = XmlHandler.deSerializeXML2ErroDTO(httpResponse.getBody());
                        response = new Response(HttpStatusCode.Conflict, erroDTO.getMensagemErro());
                        break;
                }
                return response;
            }
            @Override
            protected void onPostExecute(Response result) {
                super.onPostExecute(result);
                if (result != null) {
                    Object object = result.getBody();
                    switch (result.getStatus()){
                        case  HttpStatusCode.OK:
                            if(object instanceof Clinica) {
                                clinica = (Clinica) object;
                                setDataUI();
                            }
                            break;
                        case  HttpStatusCode.Conflict:
                            if(object instanceof String) {
                                String message = (String) object;
                                Toast.makeText(ClinicaActivity.this,message,Toast.LENGTH_LONG).show();
                            }
                            break;
                        default:
                            Toast.makeText(ClinicaActivity.this,Utils.UNKNOWN_ACTION,Toast.LENGTH_LONG).show();
                            break;
                    }
                }
                pb.setVisibility(ProgressBar.GONE);
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/getClinica/"+id);
    }
    private void postClinica2WS(Clinica clinica){
        ClinicaDTO clinicaDTO = Converter.clinica2ClinicaDTO(clinica);
        final String body =  XmlHandler.serializeClinicaDTO2XML(clinicaDTO);
        AsyncTask<String, Void, Response> task = new AsyncTask<String, Void, Response>() {
            @Override
            protected void onPreExecute() {
                pb.setVisibility(ProgressBar.VISIBLE);
            }
            @Override
            protected Response doInBackground(String... params) {
                Response response = null;
                HttpRequest httpRequest = new HttpRequest(HttpRequestType.POST,params[0],body);
                HttpResponse httpResponse = HttpConnection.makeRequest(httpRequest);
                switch (httpResponse.getStatus()){
                    case  HttpStatusCode.Created:
                        response = new Response(HttpStatusCode.Created, null);
                        break;
                    case  HttpStatusCode.Conflict:
                        ErroDTO erroDTO = XmlHandler.deSerializeXML2ErroDTO(httpResponse.getBody());
                        response = new Response(HttpStatusCode.Conflict, erroDTO.getMensagemErro());
                        break;
                }
                return response;
            }
            @Override
            protected void onPostExecute(Response result) {
                super.onPostExecute(result);
                if (result != null) {
                    Object object = result.getBody();
                    switch (result.getStatus()){
                        case  HttpStatusCode.Created:
                            Toast.makeText(ClinicaActivity.this,Utils.OPERATION_ADD_SUCESSS,Toast.LENGTH_LONG).show();
                            break;
                        case  HttpStatusCode.Conflict:
                            if(object instanceof String) {
                                String message = (String) object;
                                Toast.makeText(ClinicaActivity.this,message,Toast.LENGTH_LONG).show();
                            }
                        default:
                            Toast.makeText(ClinicaActivity.this,Utils.UNKNOWN_ACTION,Toast.LENGTH_LONG).show();
                            break;
                    }
                }
                pb.setVisibility(ProgressBar.GONE);
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica");
    }
    private void putClinica2WS(Clinica clinica) {
        ClinicaDTO clinicaDTO = Converter.clinica2ClinicaDTO(clinica);
        final String body =  XmlHandler.serializeClinicaDTO2XML(clinicaDTO);
        AsyncTask<String, Void, Response> task = new AsyncTask<String, Void, Response>() {
            @Override
            protected void onPreExecute() {
                pb.setVisibility(ProgressBar.VISIBLE);
            }
            @Override
            protected Response doInBackground(String... params) {
                Response response = null;
                HttpRequest httpRequest = new HttpRequest(HttpRequestType.PUT,params[0],body);
                HttpResponse httpResponse = HttpConnection.makeRequest(httpRequest);
                switch (httpResponse.getStatus()){
                    case  HttpStatusCode.OK:
                        response = new Response(HttpStatusCode.OK, null);
                        break;
                    case  HttpStatusCode.Conflict:
                        ErroDTO erroDTO = XmlHandler.deSerializeXML2ErroDTO(httpResponse.getBody());
                        response = new Response(HttpStatusCode.Conflict, erroDTO.getMensagemErro());
                        break;
                }
                return response;
            }
            @Override
            protected void onPostExecute(Response result) {
                super.onPostExecute(result);
                if (result != null) {
                    Object object = result.getBody();
                    switch (result.getStatus()){
                        case  HttpStatusCode.OK:
                            Toast.makeText(ClinicaActivity.this,Utils.OPERATION_UPDATE_SUCESSS,Toast.LENGTH_LONG).show();
                            break;
                        case  HttpStatusCode.Conflict:
                            if(object instanceof String) {
                                String message = (String) object;
                                Toast.makeText(ClinicaActivity.this,message,Toast.LENGTH_LONG).show();
                            }
                        default:
                            Toast.makeText(ClinicaActivity.this,Utils.UNKNOWN_ACTION,Toast.LENGTH_LONG).show();
                            break;
                    }
                }
                pb.setVisibility(ProgressBar.GONE);
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinicas/"+id);
    }
    private void deleteClinica2WS() {

        AsyncTask<String, Void, Response> task = new AsyncTask<String, Void, Response>() {
            @Override
            protected void onPreExecute() {
                pb.setVisibility(ProgressBar.VISIBLE);
            }
            @Override
            protected Response doInBackground(String... params) {
                Response response = null;
                HttpRequest httpRequest = new HttpRequest(HttpRequestType.DELETE,params[0],"");
                HttpResponse httpResponse = HttpConnection.makeRequest(httpRequest);
                switch (httpResponse.getStatus()){
                    case  HttpStatusCode.OK:
                        response = new Response(HttpStatusCode.OK, null);
                        break;
                    case  HttpStatusCode.Conflict:
                        ErroDTO erroDTO = XmlHandler.deSerializeXML2ErroDTO(httpResponse.getBody());
                        response = new Response(HttpStatusCode.Conflict, erroDTO.getMensagemErro());
                        break;
                }
                return response;
            }
            @Override
            protected void onPostExecute(Response result) {
                super.onPostExecute(result);
                if (result != null) {
                    Object object = result.getBody();
                    switch (result.getStatus()){
                        case  HttpStatusCode.OK:
                            Toast.makeText(ClinicaActivity.this,Utils.OPERATION_DELETE_SUCESSS,Toast.LENGTH_LONG).show();
                            break;
                        case  HttpStatusCode.Conflict:
                            if(object instanceof String) {
                                String message = (String) object;
                                Toast.makeText(ClinicaActivity.this,message,Toast.LENGTH_LONG).show();
                            }
                        default:
                            Toast.makeText(ClinicaActivity.this,Utils.UNKNOWN_ACTION,Toast.LENGTH_LONG).show();
                            break;
                    }
                }
                pb.setVisibility(ProgressBar.GONE);
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica/"+id);
    }
}
