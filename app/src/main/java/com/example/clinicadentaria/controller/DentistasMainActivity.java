package com.example.clinicadentaria.controller;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.adapter.ListViewAdapterDentista;
import com.example.clinicadentaria.dto.Converter;
import com.example.clinicadentaria.dto.ErroDTO;
import com.example.clinicadentaria.dto.ListaDentistaParcialDTO;
import com.example.clinicadentaria.helper.Response;
import com.example.clinicadentaria.helper.Utils;
import com.example.clinicadentaria.model.partial.DentistaParcial;
import com.example.clinicadentaria.model.partial.ListaDentistaParcial;
import com.example.clinicadentaria.network.*;
import com.example.clinicadentaria.xml.XmlHandler;
import java.util.ArrayList;

public class DentistasMainActivity extends AppCompatActivity {

    ListView lv;
    ProgressBar pb;
    ArrayList<DentistaParcial> dentistaParciais;
    ListViewAdapterDentista adapter;
    EditText etNifDentista;
    Button btEscolherNif;
    long nifClinica;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dentistas_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        pb = findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.INVISIBLE);

        lv = findViewById(R.id.listView);
        dentistaParciais = new ArrayList<DentistaParcial>();
        adapter = new ListViewAdapterDentista(this, R.layout.listview_dentistas_item, dentistaParciais);
        lv.setAdapter(adapter);
        registerForContextMenu(lv);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(DentistasMainActivity.this,"Click: details", Toast.LENGTH_SHORT).show();
                DentistaParcial dentistaParcial = (DentistaParcial) adapter.getItem(i);
                Intent intent = new Intent(DentistasMainActivity.this, DentistaActivity.class);
                intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_DETAILS);
                intent.putExtra(Utils.ID, dentistaParcial.getCedula());
                startActivity(intent);
            }
        });
        nifClinica = Long.parseLong(etNifDentista.getText().toString());
        btEscolherNif = findViewById(R.id.btEscolherNif);
        btEscolherNif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDentistasFromWS();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.add:
                Toast.makeText(DentistasMainActivity.this,"Adicionar",Toast.LENGTH_SHORT).show();
                intent = new Intent(DentistasMainActivity.this, DentistaActivity.class);
                intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_ADDING);
                startActivityForResult(intent, Utils.REQUEST_CODE_ADD_ACTIVITY);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int pos = info.position;
        Intent intent;
        DentistaParcial dentistaParcial = (DentistaParcial) adapter.getItem(pos);
        if (dentistaParcial != null) {
            switch (item.getItemId()) {
                case R.id.delete:
                    Toast.makeText(DentistasMainActivity.this,"Apagar",Toast.LENGTH_SHORT).show();
                    intent = new Intent(DentistasMainActivity.this, DentistaActivity.class);
                    intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_DELETING);
                    intent.putExtra(Utils.ID, dentistaParcial.getCedula());
                    startActivityForResult(intent, Utils.REQUEST_CODE_DELETE_ACTIVITY);
                    return true;
                case R.id.edit:
                    Toast.makeText(DentistasMainActivity.this,"Context menu: Edit",Toast.LENGTH_SHORT).show();
                    intent = new Intent(DentistasMainActivity.this, DentistaActivity.class);
                    intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_EDITING);
                    intent.putExtra(Utils.ID, dentistaParcial.getCedula());
                    startActivityForResult(intent, Utils.REQUEST_CODE_EDIT_ACTIVITY);
                    return true;
                default:
                    break;
            }
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Utils.REQUEST_CODE_ADD_ACTIVITY:
            case Utils.REQUEST_CODE_DELETE_ACTIVITY:
            case Utils.REQUEST_CODE_EDIT_ACTIVITY:
                if (resultCode == Activity.RESULT_OK) {
                    getDentistasFromWS();
                }
                break;
            default:
        }
    }

    private void getDentistasFromWS() {
        AsyncTask<String, Void, Response> task = new AsyncTask<String, Void, Response>() {
            @Override
            protected void onPreExecute() {
                pb.setVisibility(ProgressBar.VISIBLE);
            }
            @Override
            protected Response doInBackground(String... params) {
                Response response = null;
                HttpRequest httpRequest = new HttpRequest(HttpRequestType.GET,params[0],"");
                HttpResponse httpResponse = HttpConnection.makeRequest(httpRequest);
                switch (httpResponse.getStatus()){
                    case  HttpStatusCode.OK:
                        ListaDentistaParcialDTO listaDentistaParcialDTO = XmlHandler.deSerializeXML2ListaDentistaParcialDTO(httpResponse.getBody());
                        ListaDentistaParcial dentistas = Converter.listaDentistaParcialDTO2ListaDentistaParcial(listaDentistaParcialDTO);
                        response = new Response(HttpStatusCode.OK, dentistas);
                        break;
                    case  HttpStatusCode.Conflict:
                        ErroDTO erroDTO = XmlHandler.deSerializeXML2ErroDTO(httpResponse.getBody());
                        response = new Response(HttpStatusCode.Conflict, erroDTO.getMensagemErro());
                        break;
                }
                return response;
            }
            @Override
            protected void onPostExecute(Response result) {
                super.onPostExecute(result);
                dentistaParciais.clear();
                if (result != null) {
                    Object object = result.getBody();
                    switch (result.getStatus()){
                        case  HttpStatusCode.OK:
                            if(object instanceof ListaDentistaParcial) {
                                dentistaParciais.addAll(((ListaDentistaParcial) object).getDentistasParciais());
                                adapter.notifyDataSetChanged();
                            }
                            break;
                        case  HttpStatusCode.Conflict:
                            if(object instanceof String) {
                                String message = (String) object;
                                Toast.makeText(DentistasMainActivity.this,message,Toast.LENGTH_LONG).show();
                            }
                            break;
                        default:
                            Toast.makeText(DentistasMainActivity.this,Utils.UNKNOWN_ACTION,Toast.LENGTH_LONG).show();
                            break;
                    }
                }
                pb.setVisibility(ProgressBar.GONE);
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica/"+nifClinica+"/dentistas");
    }
}
